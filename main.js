#!/bin/node
const fs = require('fs');
const Discord = require('discord.js');
const path = require('path');

const TOKEN = fs.readFileSync("/etc/Pierrot/TOKEN", 'utf8')

console.log(TOKEN);

function playSound(message, sound){
    var VC = message.member.voiceChannel;
    if (!VC)
        return message.reply("Rentre dans un salon vocal chacal ou j'vais te goumer !")
    VC.join()
    .then(connection => {
        const dispatcher = connection.playFile(sound);
        dispatcher.on("end", end => {VC.leave()});
    })
    .catch(console.error);
}

const client = new Discord.Client();

client.once('ready', () => {
	console.log('Ready!');
});

client.login(TOKEN);

client.on('message', message => {
    var content = message.content;
    var author  = message.author;
    var channel = message.channel
    var member  = message.member

    if (content === ',ping'){
        channel.send("Pong !")
    }
    else if (content === ',help'){
        let aide = `Listes des commandes
\`\`\`
,ping           : Vérifie si le bot marche
,vocaltrash     : Pierrot viens d'insulter
,trash          : Envoie un message trash
,search <qqch>  : Explique comment trouver qqch sur internet
,rené <son>     : Cite rené en vocal
,marant         : Ah c'est marant
,jadorir        : J'aDoRe RiRe
,bisous         : Fait bisous chacal
,pseudo         : Changer son nom
,potage         : Y'a une couille dans le potage
,paris <langue> : Bonsoir Paris !
,ph             : ( ͡° ͜ʖ ͡°)
,clément        : nani ???
,cheh           : CHEH !
\`\`\`
`
        channel.send(aide)
    }
    else if (content === ',trash'){
        let trash   = [
        "Je vais te faire du sale",
        "Suce moi",
        "Touche ma grosse bite",
        "Gobe mes baloches",
        "Attrape mon zboub",
        "Je vais te faire monter au septième ciel bébé",
        "Prépare ton petit anus étriqué de vierge gamin",
        "Je suis là pour 2 choses : Ton cul et ton anus poupée",
        "Je vais te montrer mon légendaire brakmar"
        ];
        let choosen = trash[Math.floor(Math.random()*trash.length)];
        channel.send(choosen)
    }
    else if (content.split(' ')[0] === ',paris'){
        if (content === ',paris'){
            playSound(message, '/etc/Pierrot/Sounds/bilal/fr.mp3');
        }
        else if (["ar", "cs", "de", "en", "es", "fr", "gr", "hi", "hw", "it", "jp", "ko", "nl", "no", "pl", "pt", "ru", "th", "tl", "vi", "zh"].includes(content.split(' ')[1])){
            playSound(message, '/etc/Pierrot/Sounds/bilal/' + content.split(' ')[1] + '.mp3');
        }
        else{
            channel.send("Cette langue n'est pas dans la base de donnée")
        }
    }
    else if (content.split(' ')[0] === ',search'){
        channel.send("https://lmgtfy.com/?q=" + content.split(' ').slice(1).join('+') + "&s=d")
    }
    else if (content.split(' ')[0] === ',cheh'){
        playSound(message, '/etc/Pierrot/Sounds/cheh.mp3');
    }
    else if (content === ',marant'){
        playSound(message, '/etc/Pierrot/Sounds/cMarrant.mp3');
    }
    else if (content === ',ph'){
        playSound(message, '/etc/Pierrot/Sounds/ph.mp3');
    }
    else if (content === ',potage'){
        playSound(message, '/etc/Pierrot/Sounds/couille-potage.mp3');
    } 
    else if (content === ',jadorir'){
        playSound(message, '/etc/Pierrot/Sounds/jadorire.mp3');
    }
    else if (content === ',clément'){
        playSound(message, '/etc/Pierrot/Sounds/clement.mp3');
    }
    else if (content === ',vocaltrash'){
        var files = fs.readdirSync('/etc/Pierrot/Sounds/Trash');
        sound = '/etc/Pierrot/Sounds/Trash/' + files[Math.floor(Math.random()*files.length)];
        playSound(message, sound)
    }
    else if (content.split(' ')[0] === ',rené'){
        if (content.split(' ')[1] === 'random'){
            var files = fs.readdirSync('/etc/Pierrot/Sounds/René');
            sound = '/etc/Pierrot/Sounds/René/' + files[Math.floor(Math.random()*files.length)];
            playSound(message, sound)
        }
        else {
            var files = fs.readdirSync('/etc/Pierrot/Sounds/René');
            if (files.includes(content.split(' ')[1]) === false){
                let aide = `Liste des sons disponibles
\`\`\`
balekouille
colèreNoire
conDeTesMorts
honteux
\`\`\`
`
                channel.send(aide)
            }
            else{
                playSound(message, '/etc/Pierrot/Sounds/René/' + content.split(' ')[1] + '.mp3')
            }
        }
    }
    else if (content === ',ducul'){
        playSound(message, '/etc/Pierrot/Sounds/ducul.mp3')
    }
    else if (content === ',bisous'){
        playSound(message, '/etc/Pierrot/Sounds/bisous.mp3')
    }
    else if (content.split(' ')[0] === ',pseudo'){
        channel.send("|| Si ça marche pas faut se plaindre au admins ||")
        member.setNickname(content.replace(',pseudo ', ''));
        channel.send("Et voilà j'ai changé ton pseudo !");
    }
});
